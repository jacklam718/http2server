import 'babel-polyfill'
import {readFileSync} from 'fs'
import connect from 'connect'
import {logger} from './middleware/logger'
import {allowCors} from './middleware/allowCors'
import {allowedMethods} from './middleware/allowedMethods'
import {serviceWorkerScope} from './middleware/serviceWorkerScope'
import {sendStatic} from './middleware/sendStatic'
import {onlyAcceptTypes} from './middleware/onlyAcceptTypes'
import {blacklist} from './middleware/blacklist'
import {fallbackPush} from './middleware/fallbackPush'
import {quietErrors} from './middleware/quietErrors'

const http2 = require('http').HTTP2 || require('http2-ponyfill')

export function server ({
  root, fallback, include, exclude, silent,
  cert, cors, key, scope, immutable, worker
}) {
  const app = connect()

  app.use(logger())
  app.use(allowCors(cors))
  app.use(allowedMethods(['GET']))
  app.use(serviceWorkerScope(scope))
  app.use(sendStatic({root, immutable}))
  app.use(onlyAcceptTypes(['html']))
  app.use(blacklist(['/favicon.ico', '/robots.txt', '/sitemap.xml']))
  app.use(fallbackPush({root, immutable, fallback, include, exclude, worker}))
  app.use(quietErrors())

  return http2.createSecureServer({
    key: readFileSync(key),
    cert: readFileSync(cert)
  }, app)
}
