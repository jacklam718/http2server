import {stat} from 'fs'
import {resolveToFile} from '../helpers/resolveToFile'
import {fileStream} from '../helpers/fileStream'
import promisify from 'pify'

export function sendStatic (options) {
  return async (request, response, next) => {
    let filepath, filestats
    try {
      filepath = resolveToFile(request, options.root)
      filestats = await promisify(stat)(filepath)
      if (!filestats.isFile()) throw new Error('Not a file')
    } catch (error) {
      next()
      return
    }

    const {headers, file} = await fileStream(request, filepath, options)
    response.writeHead(200, headers)
    file.pipe(response)
  }
}
