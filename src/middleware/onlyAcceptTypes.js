import accepts from 'accepts'
import {NotAcceptable} from 'http-errors'

export function onlyAcceptTypes (types = []) {
  return (request, response, next) => {
    const accepted = accepts(request)
    for (const type of types) {
      if (accepted.type(type)) {
        next()
        return
      }
    }
    next(new NotAcceptable())
  }
}
