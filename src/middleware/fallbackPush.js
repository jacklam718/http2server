import debug from 'debug'
import {queue} from 'd3-queue'
import Cookies from 'cookies'
import {computeDigestValue} from 'cache-digest'
import {fileStream} from '../helpers/fileStream'
import {getPushPolicy} from '../helpers/getPushPolicy'
import {getDependencies} from '../helpers/getDependencies'
import {cacheDigestFilter} from '../helpers/cacheDigestFilter'
import {getOrigin} from '../helpers/getOrigin'

export function fallbackPush ({root, immutable, fallback, include, exclude, worker}) {
  const log = debug('http2server')
  return async (request, response, next) => {
    const {headers, file} = await fileStream(request, fallback, {immutable})
    const pushPolicy = getPushPolicy(
      request,
      ['fast-load', 'default', 'head', 'none']
    )
    if (pushPolicy) headers['push-policy'] = pushPolicy
    const doPush = pushPolicy !== 'none' && !!response.push
    if (!doPush) {
      response.writeHead(200, headers)
      file.pipe(response)
      return
    }

    const baseUrl = getOrigin(request)
    const cookies = new Cookies(request, response)
    const pushQueue = queue(100)
    const pushPromises = []

    const dependencies = (await getDependencies({include, exclude, root, worker}))
      .filter(cacheDigestFilter(request, cookies, baseUrl))
      .filter(({absolute}) => absolute !== fallback)

    for (const dependency of dependencies) {
      log(`PUSH ${dependency.pathname}`)
      try {
        const stream = await fileStream(request, dependency.absolute, {immutable})
        const pushResponse = response.push(encodeURI(dependency.pathname))
        pushPromises.push({pushResponse, dependency, stream})
      } catch (error) { continue }
    }

    if (request.headers['cache-response']) {
      if (cookies.get('cache-digest')) {
        cookies.set('cache-digest')
      }
    } else if (!cookies.get('cache-digest')) {
      const urls = pushPromises
        .filter(({stream: {isImmutable}}) => isImmutable)
        .map(({dependency: {pathname}}) => [baseUrl + pathname, null])
      const digestValue = computeDigestValue(false, urls, 2 ** 7)
      const cookie = Buffer.from(digestValue).toString('base64').replace(/=+$/, '')
      cookies.set('cache-digest', cookie)
    }

    response.writeHead(200, headers)
    file.pipe(response)

    for (const {pushResponse, stream} of pushPromises) {
      pushQueue.defer(async (callback) => {
        try {
          pushResponse.writeHead(200, stream.headers)
          if (pushPolicy === 'head') {
            pushResponse.end()
            callback()
          } else {
            stream.file.pipe(pushResponse)
            stream.file.on('error', () => {
              try {
                pushResponse.stream.end()
              } catch (error) {}
              callback()
            })
            pushResponse.stream.on('error', () => callback())
            pushResponse.stream.on('finish', () => callback())
          }
        } catch (error) { callback() }
        return {
          abort: () => {
            try {
              pushResponse.stream.end()
            } catch (error) {}
          }
        }
      })
    }
  }
}

process.on('unhandledRejection', console.trace)
