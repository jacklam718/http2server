export function allowCors (allowed) {
  return (request, response, next) => {
    if (allowed) {
      response.setHeader('Access-Control-Allow-Origin', '*')
    }
    next()
  }
}
