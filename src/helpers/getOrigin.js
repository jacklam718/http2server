export function getOrigin (request) {
  const protocol = request.headers['x-forwarded-proto'] ||
    request.headers[':scheme'] ||
    'https'

  const host = request.headers['x-forwarded-host'] ||
    request.headers[':authority'] ||
    request.headers['host']

  return `${protocol}://${host}`
}
