import {statSync} from 'fs'

export function directoryExists (path) {
  try {
    const stats = statSync(path)
    return stats.isDirectory()
  } catch (err) {
    return false
  }
}
