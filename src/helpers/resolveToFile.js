import parseUrl from 'parseurl'
import resolvePath from 'resolve-path'

export function resolveToFile (request, root) {
  const url = parseUrl(request)
  const pathname = decodeURIComponent(url.pathname)
  const relative = pathname.substr(1)
  const filepath = resolvePath(root, relative)
  return filepath
}
