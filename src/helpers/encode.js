import {createReadStream} from 'fs'
import {PassThrough} from 'stream'
import {freemem} from 'os'
import {delimiter} from 'path'
import LRU from 'lru-cache'
import streamToPromise from 'stream-to-promise'
import {encoder} from './encoder'

const cache = LRU({
  max: Math.min(100e6, freemem() / 2),
  length: ({length}) => length
})

export async function encode (source, encoding) {
  const key = encoding + delimiter + source

  let buffer
  if (cache.has(key)) {
    buffer = cache.get(key)
  } else {
    const input = createReadStream(source)
    const encoderStream = encoder(encoding)
    const encoderPromise = streamToPromise(encoderStream)
    input.pipe(encoderStream)
    buffer = await encoderPromise
    cache.set(key, buffer)
  }

  const stream = new PassThrough()
  stream.end(buffer)

  return {
    stream,
    length: buffer.length
  }
}
