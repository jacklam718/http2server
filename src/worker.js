import {server} from './server'

process.on('message', (argv) => {
  const app = server(argv)
  app.listen(argv.port)
})
