import cluster from 'cluster'
import physicalCpuCount from 'physical-cpu-count'
import eventToPromise from 'event-to-promise'

export async function master (argv) {
  const workers = []
  cluster.setupMaster({exec: require.resolve('./worker.js')})
  while (workers.length < physicalCpuCount) workers.push(cluster.fork())
  await Promise.all(workers.map((worker) => eventToPromise(worker, 'online')))
  workers.forEach((worker) => worker.send(argv))
  await Promise.all(workers.map((worker) => eventToPromise(worker, 'listening')))
}
